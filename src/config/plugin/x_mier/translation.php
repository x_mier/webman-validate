<?php
return [
    'locale' => 'zh_CN',
    'fallback_locale' => ['zh_CN', 'en'],
    'path' => base_path() . '/config/plugin/x_mier/validate/resource/translations',
];